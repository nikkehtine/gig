# gig

Command-line .gitignore generator

## Usage

```
gig <command> or [<argument> ...]
```

### Available commands

- `help` - print out a help message
- `list` - list all available arguments
