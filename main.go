package main

import (
	"fmt"
	"os"
)

var filename string = ".gitignore"
var api string = "https://api.github.com/repos/github/gitignore/git/trees/main"

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	args := os.Args[1:]

	if len(args) == 0 {
		fmt.Println("Error: No arguments specified")
		os.Exit(1)
	}

	if args[0] == "help" {
		if len(args) > 1 {
			fmt.Println("Error: Too many arguments")
			os.Exit(1)
		}
		fmt.Println("gig version 0.0.1\n\nUsage: gig <command> or [<argument> ...]\n\ngig accepts multiple arguments i.e. languages, tools, operating systems, editors, IDEs, etc.\n\nCommands:\n\thelp\tShows help\n\tlist\tLists all available software and languages")
		os.Exit(0)
	}

	fmt.Println("Not implemented yet.")
	os.Exit(0)

}
